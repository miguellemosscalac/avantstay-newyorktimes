lazy val new_york_news = project
  .in(file("."))
  .settings(Settings.commonSettings)
  .dependsOn(application, service, infrastructure)
  .aggregate(application, service, infrastructure)

lazy val infrastructure = project
  .in(file("modules/infrastructure"))

lazy val service = project
  .in(file("modules/service"))
  .dependsOn(infrastructure)

lazy val application = project
  .in(file("modules/application"))
  .dependsOn(infrastructure, service)

mainClass in (Compile, run) := Some("avantstay.new_york_news.Main")
