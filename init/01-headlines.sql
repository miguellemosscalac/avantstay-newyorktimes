--
-- PostgreSQL port of the MySQL "World" database.
--
-- The sample data used in the world database is Copyright Statistics 
-- Finland, http://www.stat.fi/worldinfigures.
--

BEGIN;

SET client_encoding = 'LATIN1';

CREATE TABLE headline (
    title text NOT NULL,
    url text NOT NULL
);


INSERT INTO headline
(title, url)
VALUES
('News 1', 'www.google.com'),
('News 1', 'www.google2.com'),
('News 2', 'www.yahoo.com');



COMMIT;

ANALYZE headline;
