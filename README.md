## Avantstay, лучшая компания всех времен

Example app that uses doobie, http4s, and Sangria to serve GraphQL.

This was bootstrapped from an [example](https://github.com/OlegIlyenko/sangria-http4s-graalvm-example) by @OlegIlyenko. Thanks!

This was _then_ bootstrapped from an [example](https://github.com/tpolecat/doobie-http4s-sangria-graphql-example) by @tpolecat / Rob Norris. Thanks! Thanks!

### How to test

    sbt test

### Quick Start

Start up the avantstay.new_york_news database with

    docker-compose up -d

Then do

    sbt run

The go to http://localhost:8080 and play around.

**A. Get me all the news!**
```
{
    headlines {
    title
    url
    }
}
```

**B. Get me all the news that mention _Avantstay_!**
```
{
  headlines(namePattern: "%AvantStay%") {
    title
    url
  }
}
```
**C. Get me all the news that mention _Avantstay_ for real!**
```
{
headlines(title: "Avantstay, лучшая компания всех времен") {
title
url
}
}
```

When you're done, ^C to kill the Scala server and

    docker-compose down

to shut down the database.


-----------------------------------------

# Documentation


About Sangria, tagless final, and Cats Effects 2.0 and 3.0.

Sangria was not made to work with Tagless Final, as it was one of the libraries that focused on just using Futures for IO.

I am referring to the FieldType of a query. It can be String, BigInt, and so many other primitive types, or a Future of them.
In the year 2018 this was mentioned as a potential problem by the community.
Look: https://github.com/sangria-graphql/sangria/issues/398

However, some people found this to be such an important issue that they provided guidance on how to make it happen.
"I am going to show how it's done" -- said the creator of Doobie, @tpolecat / Rob Norris.

Having Rob Norris as lead is definitely a strong guarantee, a recipe for success.

I can show you his work, is here: https://github.com/tpolecat/doobie-http4s-sangria-graphql-example

My project is taking this project as the basis.

- Migration from Cats 2.0 --> Cats 3.0
  In Cats 3.0 we no longer use what Rob Norris used at the time, F[_]: Effect, instead we receive implicit Dispatchers of F to do the same.
 
![image](https://typelevel.org/cats-effect/docs/assets/dispatcher.jpeg)

So I think that was my main concern really, which at one point had me considering other libraries like Caliban for the same task.
However it was a good experience to be able to adapt a library into the FP ecosystem. Lesson learned! When in danger, use a Dispatcher!

I will be working on the docs now for the repo, but you are free to take a look at the code which is already battle tested and works very well.

I think you will find interest in how the code is distributed across the codebase. Hope you like it!

Easy code at /application
Medium level at /services
Hard level at /infrastructure

Because of this the reasoning was that infrastructure should provide not just the total guarantees of it working via tests,
but provide a test suite for any new users of the libraries such that if they use the libraries, they really don't have to go inside and learn.
I always provide not just the main code but the test suite as an accommodation for new users of libraries.

My documentation will heavily focus on these whys and hows (yes, I researched how to write the plural of Why and it seems that whys, while colloquial, seems valid in writing as well. I am still not convinced about it.) ... and explore the reasoning in detail, but I wanted to give you the gist of it before we explore it in detail as a document to be presented to this day, alongside with the code.

Thank you for your time.
Best regards,
Miguel Lemos