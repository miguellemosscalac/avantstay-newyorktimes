// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite.contexts

import cats.data.OptionT
import cats.effect._
import cats.implicits.toFunctorOps
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.suite._
import io.circe._
import org.http4s.Method.POST
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits.http4sLiteralsSyntax

object Integration {
  import Protocol._

  class Integration[F[_]: Async, Repository[G[_]]](
      sangriaGraphQLServer: SangriaGraphQLServer[F, Repository]
  ) extends TestSuiteContext[F] {
    def queryExecutor[A: Decoder](query: Query[A]): OptionT[F, Seq[A]] =
      for {
        response <- sangriaGraphQLServer.routes
          .run(
            Request[F](
              POST,
              uri"/graphql"
            ).withEntity[String](query.`as GraphQL query`)
          )
        json <- OptionT pure response.asJson
        optionA <- OptionT liftF json
          .map(
            extractFromResponse[A](query)
          )
        a <- OptionT fromOption optionA
      } yield a
  }
}
