// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.utils

object GraphQLSyntax {

  implicit class `GraphQlSintax`(s: String) {

    private def oneliner: String => String =
      s =>
        s.stripMargin
          .replaceAll("\n", " ")
          .replaceAll("\"", "\\\\\"")

    def `as GraphQL query`: String = {
      s"""{"query": "${oneliner(s)}"}"""
    }
    import io.circe.parser._

    def asJson = parse(`as GraphQL query`)
  }

}
