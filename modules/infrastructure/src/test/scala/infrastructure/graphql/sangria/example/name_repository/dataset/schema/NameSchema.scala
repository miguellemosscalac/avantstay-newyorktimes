// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.example.name_repository.dataset.schema

import cats.effect.std.Dispatcher
import infrastructure.graphql.sangria.example.name_repository.dataset.model.Name
import infrastructure.graphql.sangria.example.name_repository.dataset.repo.NameRepository
import sangria.schema.{fields, Argument, Field, ListType, ObjectType, OptionType, Schema, StringType}

private[sangria] object NameSchema {

  object NameType {

    def apply[F[_]]: ObjectType[NameRepository[F], Name] =
      ObjectType(
        name = "Name",
        fieldsFn = () =>
          fields(
            Field(name = "name", fieldType = StringType, description = Some("name"), resolve = _.value.name),
            Field(name = "surname", fieldType = StringType, description = Some("surname"), resolve = _.value.surname)
          )
      )
  }

  val Name: Argument[String] =
    Argument(
      name = "name",
      argumentType = StringType,
      description = "Name"
    )

  def apply[F[_]](
      implicit dispatcher: Dispatcher[F]
  ): ObjectType[NameRepository[F], scala.Unit] =
    ObjectType(
      name = "Query",
      fields = fields(
        Field(
          name = "names",
          fieldType = ListType(NameType[F]),
          description = Some("Returns all names"),
          resolve = c => dispatcher.unsafeToFuture(c.ctx.getAll)
        ),
        Field(
          name = "names_by_surname",
          fieldType = OptionType(NameType[F]),
          description = Some("Returns names with the given surname, if any."),
          arguments = List(Name),
          resolve = c => dispatcher.unsafeToFuture(c.ctx.getByName(c.arg(Name)))
        )
      )
    )

  def schema[F[_]](
      implicit dispatcher: Dispatcher[F]
  ): Schema[NameRepository[F], scala.Unit] =
    Schema(NameSchema[F])

}
