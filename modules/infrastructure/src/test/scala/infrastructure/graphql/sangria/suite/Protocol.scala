// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite

import io.circe.{Decoder, Json}

object Protocol {
  sealed trait Query[A] {
    val name: String
    val query: String
    val expected: Seq[A]
  }
  case class SomeQuery[A](
      name: String,
      query: String,
      expected: Seq[A]
  ) extends Query[A]
  case class ListQuery[A](
      name: String,
      query: String,
      expected: Seq[A]
  ) extends Query[A]
  case class QueryWithoutSpecificExpectation[A](
      name: String,
      query: String
  ) extends Query[A] {
    val expected: Seq[A] = Seq.empty
  }

  implicit class `Query as GraphQL query`[A](query: Query[A]) {
    import infrastructure.graphql.sangria.utils.GraphQLSyntax._
    def `as GraphQL query` = query.query.`as GraphQL query`
    def `as GraphQL query in Json format` = query.query.asJson
  }

  def extractFromResponse[A: Decoder](query: Query[A])(response: Json): Option[Seq[A]] = {
    query match {
      case SomeQuery(name, query, expected) =>
        response.hcursor.downField("data").get[A](name).toOption.map(Seq(_))
      case ListQuery(name, query, expected) =>
        response.hcursor.downField("data").get[Seq[A]](name).toOption
      case QueryWithoutSpecificExpectation(name, query) =>
        response.hcursor.downField("data").get[Seq[A]](name).toOption

    }
  }
}
