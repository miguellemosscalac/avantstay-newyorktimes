// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite

import cats.data.OptionT
import cats.effect._
import cats.effect.std.Dispatcher
import cats.effect.testing.UnsafeRun
import cats.effect.testing.specs2._
import infrastructure.graphql.sangria.SangriaGraphQLServer
import io.circe.Decoder
import org.specs2.mutable.SpecificationLike

abstract class TestSuite[
    F[_]: Async: UnsafeRun,
    Dataset: Decoder,
    Repository[G[_]]
](
    sangriaGraphQLServer: Dispatcher[F] => SangriaGraphQLServer[F, Repository],
    val context: SangriaGraphQLServer[F, Repository] => TestSuiteContext[F],
    queries: Seq[Protocol.Query[Dataset]]
) extends CatsResource[F, SangriaGraphQLServer[F, Repository]]
    with SpecificationLike {

  val resource: Resource[F, SangriaGraphQLServer[F, Repository]] =
    Dispatcher[F] map sangriaGraphQLServer

  import cats.implicits._

  "GraphQL queries are answered as expected" in withResource { ref =>
    queries
      .map { query =>
        context(ref)
          .queryExecutor(query)
          .value
          .map { response =>
            response mustEqual Some(query.expected)
          }
      }
      .sequence
      .map(_.fold(0 mustEqual (0))(_ and _))

  }

}
