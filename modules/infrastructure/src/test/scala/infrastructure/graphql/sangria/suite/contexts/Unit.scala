// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite.contexts

import cats.data.OptionT
import cats.effect._
import cats.implicits.toFunctorOps
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.suite.Protocol.{extractFromResponse, Query}
import infrastructure.graphql.sangria.suite._
import io.circe.Decoder

object Unit {
  class Unit[F[_]: Async, Repository[G[_]]](
      sangriaGraphQLServer: SangriaGraphQLServer[F, Repository]
  ) extends TestSuiteContext[F] {
    def queryExecutor[A: Decoder](query: Query[A]): OptionT[F, Seq[A]] = {
      OptionT apply sangriaGraphQLServer
        .graphQL(scala.concurrent.ExecutionContext.global)
        .query {
          val json = query.`as GraphQL query in Json format`
          json.toOption.get
        }
        .map { _.toOption.flatMap { extractFromResponse[A](query) } }
    }
  }
}
