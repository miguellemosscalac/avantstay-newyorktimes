// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite.contexts

import cats.data.OptionT
import cats.effect._
import cats.implicits.toFunctorOps
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.suite._
import io.circe._
import org.http4s.Method.POST
import org.http4s._
import org.http4s.blaze.client.BlazeClientBuilder
import org.http4s.server.Server

object EndToEnd {
  import Protocol._

  class EndToEnd[F[_]: Async, Repository[G[_]]](
      sangriaGraphQLServer: SangriaGraphQLServer[F, Repository]
  ) extends TestSuiteContext[F] {
    private val server: Resource[F, Server] =
      sangriaGraphQLServer.resource.evalOn(scala.concurrent.ExecutionContext.global)
    private val client = BlazeClientBuilder.apply[F](scala.concurrent.ExecutionContext.global).resource

    def queryExecutor[A: Decoder](query: Query[A]): OptionT[F, Seq[A]] =
      OptionT apply
      server.use { server =>
        client.use { client =>
          client.expectOption[Seq[A]](
            Request[F](
              POST,
              server.baseUri / "graphql"
            ).withEntity[String](query.`as GraphQL query`)
          )(entityDecoder[A](query))
        }
      }

    implicit def entityDecoder[A: Decoder](query: Query[A]): EntityDecoder[F, Seq[A]] = new EntityDecoder[F, Seq[A]] {
      override def decode(media: Media[F], strict: Boolean): DecodeResult[F, Seq[A]] =
        DecodeResult.apply {
          media.bodyText.compile.string
            .map { text =>
              io.circe.parser.parse(text).toOption.flatMap { json =>
                extractFromResponse[A](query)(json)
              }
            }
            .map {
              case Some(value) => Right(value)
              case None =>
                Left(MalformedMessageBodyFailure("Failed to extract message from server response", None))
            }
        }
      override def consumes: Set[MediaRange] = Set(MediaRange.`text/*`)
    }
  }
}
