// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.example.name_repository

import cats.effect.IO
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.example.name_repository.dataset.model.Name
import infrastructure.graphql.sangria.example.name_repository.dataset.repo.NameRepository
import infrastructure.graphql.sangria.example.name_repository.dataset.schema.NameSchema
import infrastructure.graphql.sangria.suite.{Protocol, TestSuiteContext, TestSuite => SangriaTestSuite}

private[sangria] abstract class TestSuite(
    context: SangriaGraphQLServer[IO, NameRepository] => TestSuiteContext[IO]
) extends SangriaTestSuite[IO, Name, NameRepository](
      context = context,
      sangriaGraphQLServer = { implicit dispatcher =>
        new SangriaGraphQLServer(
          NameSchema.schema.query,
          new NameRepository
        )
      },
      queries = Seq(
        Protocol.ListQuery[Name](
          name = "names",
          query = """
          {
            names {
              name
              surname
            }
          }
          """,
          expected = Seq(Name("Thomas", "Kant"), Name("Alph", "ET"))
        ),
        Protocol.SomeQuery[Name](
          name = "names_by_surname",
          query = """
          {
            names_by_surname(name: "Thomas") {
              name
              surname
            }
          }
          """,
          expected = Seq(Name("Thomas", "Kant"))
        )
      )
    )
