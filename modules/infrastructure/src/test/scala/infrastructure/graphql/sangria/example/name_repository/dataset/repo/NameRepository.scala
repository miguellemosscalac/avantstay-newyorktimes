// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.example.name_repository.dataset.repo

import cats.Applicative
import infrastructure.graphql.sangria.example.name_repository.dataset.model.Name

private[sangria] class NameRepository[F[_]: Applicative] {
  val names = List(
    Name("Thomas", "Kant"),
    Name("Alph", "ET")
  )

  def getAll: F[List[Name]] =
    Applicative[F] pure names

  def getByName(name: String): F[Option[Name]] =
    Applicative[F] pure names.find(_.name == name)
}
