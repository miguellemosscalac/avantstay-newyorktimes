// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.example.name_repository

import cats.effect.IO
import infrastructure.graphql.sangria.example.name_repository.dataset.repo.NameRepository
import infrastructure.graphql.sangria.suite.contexts.Integration.Integration

object Integration
    extends TestSuite(
      new Integration[IO, NameRepository](_)
    )
