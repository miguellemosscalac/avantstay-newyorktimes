// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.suite

import cats.data.OptionT
import io.circe.Decoder

trait TestSuiteContext[F[_]] {
  import Protocol.Query
  def queryExecutor[A: Decoder](query: Query[A]): OptionT[F, Seq[A]]
}
