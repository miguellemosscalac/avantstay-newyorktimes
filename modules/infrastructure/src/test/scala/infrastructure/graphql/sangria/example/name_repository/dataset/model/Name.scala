// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria.example.name_repository.dataset.model

import io.circe._
import io.circe.generic.semiauto._

private[sangria] case class Name(name: String, surname: String)
private[sangria] object Name {
  implicit val nameDecoder: Decoder[Name] = deriveDecoder
  def fromJson(name: Json): Option[Name] =
    nameDecoder.decodeJson(name).toOption
}
