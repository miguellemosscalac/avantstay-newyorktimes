// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package infrastructure.graphql.sangria

import _root_.sangria.schema._
import cats.effect._
import cats.implicits._
import infrastructure.graphql.{GraphQL, GraphQLRoutes}
import org.http4s._
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.dsl._
import org.http4s.headers.Location
import org.http4s.implicits._
import org.http4s.server.Server

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global

class SangriaGraphQLServer[F[_]: Async, Repository[G[_]]](
    queryType: ObjectType[Repository[F], scala.Unit],
    val repository: Repository[F]
) {

  // Construct a GraphQL implementation based on our Sangria definitions.
  def graphQL(
      blockingContext: ExecutionContext
  ): GraphQL[F] =
    SangriaGraphQL[F].apply(
      Schema(
        query = queryType,
        mutation = None
      ),
      repository.pure[F],
      blockingContext
    )

  // Playground or else redirect to playground
  def playgroundOrElse: HttpRoutes[F] = {
    object dsl extends Http4sDsl[F]; import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / "playground.html" =>
        StaticFile
          .fromResource[F]("/assets/playground.html")
          .getOrElseF(NotFound())

      case _ =>
        PermanentRedirect(Location(Uri.uri("/playground.html")))

    }
  }

  def server(
      routes: HttpRoutes[F]
  ): Resource[F, Server] =
    BlazeServerBuilder[F](global)
      .bindHttp(8080, "localhost")
      .withHttpApp(routes.orNotFound)
      .resource

  // Resource that constructs our final routes.
  def routes: HttpRoutes[F] =
    GraphQLRoutes[F](graphQL(scala.concurrent.ExecutionContext.global)) <+> playgroundOrElse

  // Resource that constructs our final server.
  def resource: Resource[F, Server] =
    server(routes)
}
