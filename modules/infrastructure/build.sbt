lazy val catsEffectVersion = "3.2.0"
lazy val catsVersion = "2.6.1"
lazy val circeVersion = "0.14.1"
lazy val doobieVersion = "1.0.0-M5"
lazy val fs2Version = "2.3.0"
lazy val kindProjectorVersion = "0.9.10"
lazy val log4catsVersion = "1.0.1"
lazy val sangriaCirceVersion = "1.3.2"
lazy val sangriaVersion = "2.1.3"
lazy val scala12Version = "2.13.6"
lazy val slf4jVersion = "1.7.30"
lazy val zioVersion = "1.0.9"
lazy val http4sVersion = "1.0.0-M23"

//addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

lazy val infrastructure = project
  .in(file("."))
  .enablePlugins(AutomateHeaderPlugin)
  .settings(Settings.commonSettings)
  .settings(
    organization := "avantstay",
    name := "infrastructure",
    description := "Sangria example with doobie backend.",
    libraryDependencies ++= Seq(
        "org.typelevel" %% "cats-core" % catsVersion,
        "org.typelevel" %% "cats-effect" % catsEffectVersion,
        "co.fs2" %% "fs2-core" % fs2Version,
        "co.fs2" %% "fs2-io" % fs2Version,
        "org.sangria-graphql" %% "sangria" % sangriaVersion,
        "org.sangria-graphql" %% "sangria-circe" % sangriaCirceVersion,
        "org.tpolecat" %% "doobie-core" % doobieVersion,
        "org.tpolecat" %% "doobie-postgres" % doobieVersion,
        "org.tpolecat" %% "doobie-hikari" % doobieVersion,
        "org.http4s" %% "http4s-dsl" % http4sVersion,
        "org.http4s" %% "http4s-blaze-server" % http4sVersion,
        "org.http4s" %% "http4s-blaze-client" % http4sVersion,
        "org.http4s" %% "http4s-circe" % http4sVersion,
        "io.circe" %% "circe-optics" % circeVersion,
        "io.circe" %% "circe-generic" % "0.15.0-M1",
        "io.circe" %% "circe-parser" % circeVersion,
        "io.chrisdavenport" %% "log4cats-slf4j" % log4catsVersion,
        "org.slf4j" % "slf4j-simple" % slf4jVersion,
        "org.scalatest" %% "scalatest" % "3.2.9" % Test
        // https://mvnrepository.com/artifact/org.typelevel/cats-effect-testing-scalatest
        //"org.typelevel" %% "cats-effect-testing-scalatest" % "1.1.1" % Test
      ),
    libraryDependencies ++= Seq(
        "org.specs2" %% "specs2-core" % "4.12.3" % Test,
        "org.typelevel" %% "cats-effect-testing-specs2" % "1.1.1" % Test,
        "dev.zio" %% "zio-interop-cats" % "3.1.1.0",
        "dev.zio" %% "zio-test" % zioVersion % "test",
        "dev.zio" %% "zio-test-sbt" % zioVersion % "test",
        "dev.zio" %% "zio-test-magnolia" % zioVersion % "test" // optional
      ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    publishArtifact in Test := true
  )
