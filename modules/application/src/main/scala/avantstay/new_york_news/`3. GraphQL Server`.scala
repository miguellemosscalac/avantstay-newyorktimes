// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news

import avantstay.new_york_news.graphql.{HeadlineRepo, QueryType}
import cats.effect.{ExitCode, IO}
import cats.effect.std.Dispatcher
import doobie.hikari.HikariTransactor
import infrastructure.graphql.sangria.SangriaGraphQLServer

object `3. GraphQL Server` {
  def startedServer(database: HikariTransactor[IO]): IO[ExitCode] =
    for {
      started <- IO pure Dispatcher[IO]
        .flatMap { implicit dispatcher =>
          new SangriaGraphQLServer(
            QueryType[IO],
            HeadlineRepo.fromTransactor[IO].apply(database)
          ).resource
        }
      exitCode <- started.use(_ => IO.never.as(ExitCode.Success))
    } yield exitCode
}
