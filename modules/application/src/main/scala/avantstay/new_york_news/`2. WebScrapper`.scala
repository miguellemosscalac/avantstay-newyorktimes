// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news

import avantstay.new_york_news.graphql.HeadlineRepo
import avantstay.new_york_news.web_scrapping.DownloadNews
import cats.effect._
import avantstay.new_york_news.web_scrapping.DownloadNews.RecordsPersisted
import doobie.hikari.HikariTransactor

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps

object `2. WebScrapper` {

  def started(tx: HikariTransactor[IO]) =
    for {
      repository <- IO pure HeadlineRepo.fromTransactor[IO].apply(tx)
      ref <- Ref.of[IO, RecordsPersisted](RecordsPersisted(0))
      fiber <- scrapping(ref, repository, 5 minutes).start
    } yield fiber

  private def scrapping(r: Ref[IO, RecordsPersisted],
                        repository: HeadlineRepo[IO],
                        duration: FiniteDuration): IO[Unit] = {
    for {
      persisted <- DownloadNews.toRepository(repository)
      before <- r.get
      _ <- r.set(RecordsPersisted(before.amount + persisted.amount))
      after <- r.get
      _ <- IO println (s"Persisted [${persisted.amount}] more headlines. Amount of total persisted in this session: [${after}] headlines.")
      _ <- IO sleep duration
      _ <- scrapping(r, repository, duration)
    } yield ()
  }

}
