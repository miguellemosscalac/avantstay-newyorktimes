// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news

import avantstay.new_york_news.graphql.{HeadlineRepo, QueryType}
import cats.effect._
import cats.effect.std.Dispatcher
import infrastructure.graphql.sangria.SangriaGraphQLServer

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      connected <- `1. Database`.connected
      scrapping <- `2. WebScrapper`.started(connected)
      started <- IO pure Dispatcher[IO]
        .flatMap { implicit dispatcher =>
          new SangriaGraphQLServer(
            QueryType[IO],
            HeadlineRepo.fromTransactor[IO].apply(connected)
          ).resource
        }
      done <- started.use(_ => IO.never.as(ExitCode.Success))
    } yield done
  }

}
