// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news

import cats.effect.IO
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts

object `1. Database` {
  def connected =
    for {
      connection <- ExecutionContexts
        .fixedThreadPool[IO](10)
        .flatMap { ce =>
          HikariTransactor.newHikariTransactor(
            "org.postgresql.Driver",
            "jdbc:postgresql:headlines",
            "user",
            "password",
            ce
          )
        }
        .allocated
        .map(_._1)
    } yield connection
}
