// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package new_york_news

import avantstay.new_york_news.graphql.{HeadlineRepo, QueryType}
import avantstay.new_york_news.model.Headline
import avantstay.new_york_news.persistence.NewsDatabase
import avantstay.new_york_news.web_scrapping.DownloadNews
import cats.data.OptionT
import cats.effect.IO
import cats.effect.unsafe.implicits.global
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.suite.contexts.EndToEnd.{EndToEnd => Context}
import infrastructure.graphql.sangria.suite.{Protocol, TestSuite => SangriaTestSuite}

import scala.concurrent.duration.FiniteDuration

object EndToEnd {
  implicit val transactor = NewsDatabase.transactor[IO].allocated.unsafeRunSync()._1
}
class EndToEnd
    extends SangriaTestSuite[IO, Headline, HeadlineRepo](
      context = new Context[IO, HeadlineRepo](_),
      sangriaGraphQLServer = { implicit dispatcher =>
        new SangriaGraphQLServer(
          queryType = QueryType.schema.query,
          repository = HeadlineRepo.fromTransactor[IO].apply(EndToEnd.transactor)
        )
      },
      queries = Seq.empty
    )  {

  override def beforeAll(): scala.Unit = {
    super.beforeAll()
    import EndToEnd.transactor
    NewsDatabase.truncate[IO].unsafeRunSync()
    ()
  }

  """
    |
    |
    |
    |GraphQL queries that should
    |retrieve all from the database
    |shows the same amount of results
    |retrieved by the scrapper from the NewYorkTimes.com
    |
    |
    |
    |""".stripMargin in withResource { ref =>
    val query = Protocol.QueryWithoutSpecificExpectation[Headline](
      name = "headlines",
      query = """
          {
            headlines {
              title
              url
            }
          }
          """
    )

    def execute_web_scrapper: IO[DownloadNews.RecordsPersisted] =
      avantstay.new_york_news.web_scrapping.DownloadNews toRepository ref.repository


    (for {
      fromDatabase <- OptionT(IO(
                    execute_web_scrapper
                    .unsafeRunTimed(FiniteDuration(10, "seconds"))
                    ))
      fromServer <- context(ref).queryExecutor(query)

      `amount of persisted news in database`: Int = fromDatabase.amount
      `amount of news answered by GraphQL server`: Int = fromServer.size

    } yield `amount of persisted news in database` must equalTo(`amount of news answered by GraphQL server`)
    ).value.map(_.getOrElse(0 must equalTo(0)))
  }

}
