lazy val catsEffectVersion = "2.1.3"
lazy val catsVersion = "2.1.1"
lazy val circeVersion = "0.13.0"
lazy val doobieVersion = "0.9.0"
lazy val fs2Version = "2.3.0"
lazy val kindProjectorVersion = "0.9.10"
lazy val log4catsVersion = "1.0.1"
lazy val sangriaCirceVersion = "1.3.0"
lazy val sangriaVersion = "1.4.2"
lazy val http4sVersion = "0.21.4"
lazy val slf4jVersion = "1.7.30"

lazy val application = project
  .in(file("."))
  .enablePlugins(AutomateHeaderPlugin)
  .settings(Settings.commonSettings)
  .settings(
    scalaVersion := "2.13.6",
    name := "application",
    description := "Sangria example with doobie backend.",
    libraryDependencies ++= Seq(
        "avantstay" %% "infrastructure" % "0.1.0-SNAPSHOT" % "compile->compile;test->test",
        "avantstay" %% "service" % "0.1.0-SNAPSHOT" % "compile->compile;test->test",
        "org.scalatest" %% "scalatest" % "3.2.9" % Test
      )
  )
