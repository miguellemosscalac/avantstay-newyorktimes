// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package graphql

import avantstay.new_york_news.graphql.{HeadlineRepo, QueryType}
import avantstay.new_york_news.model.Headline
import avantstay.new_york_news.web_scrapping.DownloadNews
import cats.effect.IO
import infrastructure.graphql.sangria.SangriaGraphQLServer
import infrastructure.graphql.sangria.suite.{Protocol, TestSuiteContext, TestSuite => SangriaTestSuite}

abstract class TestSuite(
    context: SangriaGraphQLServer[IO, HeadlineRepo] => TestSuiteContext[IO],
    repository: HeadlineRepo[IO]
) extends SangriaTestSuite[IO, Headline, HeadlineRepo](
      context = context,
      sangriaGraphQLServer = { implicit dispatcher =>
        new SangriaGraphQLServer(QueryType.schema.query, repository)
      },
      queries = Seq(
        Protocol.ListQuery[Headline](
          name = "headlines",
          query = """
          {
            headlines(namePattern: "AvantstayNews %") {
              title
              url
            }
          }        
          """,
          expected = Dataset.headlines
        ),
        Protocol.ListQuery[Headline](
          name = "headlines_by_title",
          query = s"""
          {
            headlines_by_title(title: "${Dataset.headlines.last.title}") {
              title
              url
            }
          }        
          """,
          expected = Seq(Dataset.headlines.last)
        )
      )
    ) {
  val contextRef = context
  val web_scrapper: IO[DownloadNews.RecordsPersisted] =
    avantstay.new_york_news.web_scrapping.DownloadNews.toRepository(repository)

}
