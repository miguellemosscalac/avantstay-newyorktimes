// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package graphql

import avantstay.new_york_news.graphql.HeadlineRepo
import avantstay.new_york_news.model.Headline
import cats.effect.IO
import infrastructure.graphql.sangria.suite.contexts.Unit.{Unit => Context}

class Unit
    extends TestSuite(
      context = new Context[IO, HeadlineRepo](_),
      repository = new HeadlineRepo[IO] {
        override def fetchByPattern(pattern: Option[String]): IO[List[Headline]] =
          IO pure Dataset.headlines

        override def fetchByTitle(title: String): IO[List[Headline]] =
          IO pure Dataset.headlines.filter(_.title == title)

        override def persistBatch(headlines: Seq[Headline]): IO[Int] = IO.pure(0)
      }
    )
