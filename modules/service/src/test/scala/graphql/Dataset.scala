// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package graphql

import avantstay.new_york_news.model.Headline

object Dataset {
  val headlines: List[Headline] = List(
    Headline("AvantstayNews 1", "something happened!"),
    Headline("AvantstayNews 2", "follow up on the thing that happened. It happened again!"),
    Headline("AvantstayNews 3", "And it is happening again in other places as well! We have an invasion of balloons!"),
    Headline("AvantstayNews 4", "Balloons have been sighted in 5 countries."),
    Headline("AvantstayNews 4", "Россия опровергает сообщения о полетах воздушных шаров."),
    Headline("AvantstayNews 5", "Balloons have been sighted in 6 countries."),
    Headline("AvantstayNews 5", "Московский пресс-релиз. Путин закрывает границы. Кроме Беларуси."),
    Headline("AvantstayNews 6", "Balloonos everywhere! Mamma mia! -- says Italy correspondant."),
    Headline("AvantstayNews 7", "вторгающиеся воздушные шары !Барбарроса !")
  )
}
