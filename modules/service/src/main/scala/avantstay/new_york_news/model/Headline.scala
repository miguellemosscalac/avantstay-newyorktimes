// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.model

import io.circe.Decoder
import io.circe.generic.semiauto._

final case class Headline(
    title: String,
    url: String
)

object Headline {
  implicit val decoder: Decoder[Headline] = deriveDecoder
}
