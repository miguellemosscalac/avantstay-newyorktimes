// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.persistence

import avantstay.new_york_news.graphql.QueryType
import avantstay.new_york_news.model.Headline
import cats.effect._
import cats.effect.std.Dispatcher
import doobie.Fragment
import doobie.hikari.HikariTransactor
import doobie.implicits.toSqlInterpolator
import cats.implicits._
import doobie._
import doobie.implicits._

object NewsDatabase {

  def transactor[F[_]: Async] =
    HikariTransactor
      .newHikariTransactor[F](
        "org.postgresql.Driver",
        "jdbc:postgresql:headlines",
        "user",
        "password",
        scala.concurrent.ExecutionContext.global
      )

  def drop[F[_]: Async](
      implicit
      transactor: Transactor[F]
  ) =
    sql"""
      DROP TABLE IF EXISTS headline
      """.update.run

  def create[F[_]: Async](
      implicit
      transactor: Transactor[F]
  ) =
    sql"""
      CREATE TABLE headline (
      title text NOT NULL,
      url text NOT NULL
      )
      """.update.run

  def truncate[F[_]: Async](
      implicit
      transactor: Transactor[F]
  ) =
    (drop[F], create[F])
      .mapN(_ + _)
      .transact(transactor)

  val select: Fragment =
    fr"""
          SELECT title, url
          FROM   headline
        """
  def fetchAll[F[_]: Async](pattern: Option[String])(
      implicit
      transactor: Transactor[F]
  ): F[List[Headline]] =
    (select ++ pattern.foldMap(p => sql"WHERE title ILIKE $p")).query[Headline].to[List].transact(transactor)

  def fetchByTitle[F[_]: Async](title: String)(
      implicit
      transactor: Transactor[F]
  ): F[List[Headline]] =
    (select ++ sql"WHERE title = $title").query[Headline].to[List].transact(transactor)

  def persistBatch[F[_]: Async](headlines: Seq[Headline])(
      implicit
      transactor: Transactor[F]
  ): F[Int] = {
    type HeadlineType = (String, String)
    def insertMany(ps: List[HeadlineType]): ConnectionIO[Int] =
      Update[HeadlineType]("insert into headline (title, url) values (?, ?)").updateMany(ps)
    val batch: List[HeadlineType] = headlines.map(headline => (headline.title, headline.url)).toList
    insertMany(batch).transact(transactor)
  }
}
