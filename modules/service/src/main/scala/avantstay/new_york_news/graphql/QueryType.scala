// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.graphql

import cats.effect.std.Dispatcher
import sangria.schema._

object QueryType {

  val NamePattern: Argument[String] =
    Argument(
      name = "namePattern",
      argumentType = OptionInputType(StringType),
      description = "SQL-style pattern for headline title, like \"San %\".",
      defaultValue = "%"
    )

  val Title: Argument[String] =
    Argument(
      name = "title",
      argumentType = StringType,
      description = "Title of a headline"
    )

  def apply[F[_]](
      implicit
      dispatcher: Dispatcher[F]
  ): ObjectType[HeadlineRepo[F], Unit] =
    ObjectType(
      name = "Query",
      fields = fields(
        Field(
          name = "headlines",
          fieldType = ListType(HeadlineType[F]),
          description = Some("Returns headlines with the given name pattern, if any."),
          arguments = List(NamePattern),
          resolve = c => dispatcher unsafeToFuture c.ctx.fetchByPattern(c.argOpt(NamePattern))
        ),
        Field(
          name = "headlines_by_title",
          fieldType = ListType(HeadlineType[F]),
          description = Some("Returns headlines with the given title, if any."),
          arguments = List(Title),
          resolve = c => dispatcher unsafeToFuture c.ctx.fetchByTitle(c.arg(Title))
        )
      )
    )

  def schema[F[_]](
      implicit
      dispatcher: Dispatcher[F]
  ): Schema[HeadlineRepo[F], Unit] =
    Schema(QueryType[F])

}
