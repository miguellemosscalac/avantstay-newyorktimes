// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.graphql

import avantstay.new_york_news.model.Headline
import sangria.schema.{fields, Field, ObjectType, StringType}

object HeadlineType {

  def apply[F[_]]: ObjectType[HeadlineRepo[F], Headline] =
    ObjectType(
      name = "Headline",
      fieldsFn = () =>
        fields(
          Field(name = "title", fieldType = StringType, description = Some("Headline title."), resolve = _.value.title),
          Field(
            name = "url",
            fieldType = StringType,
            description = Some("Headline url"),
            resolve = _.value.url
          )
        )
    )

}
