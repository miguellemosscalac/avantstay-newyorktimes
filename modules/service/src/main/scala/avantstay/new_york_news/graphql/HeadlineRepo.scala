// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.graphql

import avantstay.new_york_news.model.Headline
import avantstay.new_york_news.persistence.NewsDatabase
import cats.effect._
import doobie._

trait HeadlineRepo[F[_]] {
  def persistBatch(headlines: Seq[Headline]): F[Int]
  def fetchByPattern(pattern: Option[String]): F[List[Headline]]
  def fetchByTitle(title: String): F[List[Headline]]
}
// format: off
object HeadlineRepo {

  def fromTransactor[F[_] : Async]: Transactor[F] => HeadlineRepo[F] = { implicit transactor =>

    new HeadlineRepo[F] {
      def fetchByPattern(pattern: Option[String]): F[List[Headline]] =
        NewsDatabase.fetchAll(pattern)

      def fetchByTitle(title: String): F[List[Headline]] =
        NewsDatabase.fetchByTitle(title)

      def persistBatch(headlines: Seq[Headline]): F[Int] =
        NewsDatabase.persistBatch(headlines)
    }

  }
  
}
// format: on
