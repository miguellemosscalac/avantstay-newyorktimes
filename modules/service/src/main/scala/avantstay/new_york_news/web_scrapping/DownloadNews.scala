// Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
// This software is licensed under the MIT License (MIT).
// For more information see LICENSE or https://opensource.org/licenses/MIT

package avantstay.new_york_news.web_scrapping

import avantstay.new_york_news.graphql.HeadlineRepo
import avantstay.new_york_news.model.Headline
import cats.effect.Async
import cats.implicits.toFunctorOps
import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors._

object DownloadNews {

  case class RecordsPersisted(amount: Int)
  def toRepository[F[_]: Async](repository: HeadlineRepo[F]): F[RecordsPersisted] = {
    val browser = JsoupBrowser()

    case class Url(url: String)
    case class Title(text: String)

    val headlines = browser.get("https://www.nytimes.com/") >> elementList("a h3")
        .map { elements =>
          elements
            .map { element =>
              element.parent.flatMap(a => a.attrs.get("href")) -> element.text
            }
            .collect {
              case (Some(url), text) if text.nonEmpty => Headline(url, text)
            }
        }

    repository
      .persistBatch(headlines)
      .map(RecordsPersisted)

  }

}
