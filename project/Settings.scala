import sbt._
import sbt.Keys._

import de.heikoseeberger.sbtheader.HeaderPlugin.autoImport.{
  headerLicense,
  headerMappings,
  HeaderCommentStyle,
  HeaderFileType,
  HeaderLicense
}
import sbt.Keys.{console, licenses, organization, scalaVersion, scalacOptions}
import sbt.{addCompilerPlugin, url, Compile}

object Settings {

  lazy val kindProjectorVersion = "0.9.10"

  lazy val commonSettings = Seq(
    organization := "avantstay",
    licenses ++= Seq(("MIT", url("http://opensource.org/licenses/MIT"))),
    scalaVersion := "2.13.6",
    headerMappings := headerMappings.value + (HeaderFileType.scala -> HeaderCommentStyle.cppStyleLineComment),
    headerLicense := Some(
        HeaderLicense.Custom(
          """|Copyright (c) 2021 by Rob Norris & Miguel Lemos :)
           |This software is licensed under the MIT License (MIT).
           |For more information see LICENSE or https://opensource.org/licenses/MIT
           |""".stripMargin
        )
      )
  )
}
